import {createApp} from 'vue'
import App from './App.vue'

import './assets/formulate.css'

import VueFormGenerator from "vue-form-generator";
import "vue-form-generator/dist/vfg-core.css";

const app = createApp(App);
app.use(VueFormGenerator);



app.mount('#app');
